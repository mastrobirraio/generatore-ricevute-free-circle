<?php namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

use App\Helper\ACL;
use App\Helper\Password;
use App\Helper\Session;
use App\Model\UserModel;


class UserController
{

    public function getLogin(Request $request, Response $response, Twig $twig)
    {
        return $twig->render($response, "login.twig");
    }

    public function postLogin(Request $request, Response $response, Twig $twig)
    {
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $user = $this->getUser($username, $password);

        if (!isset($user)) {
            return $response->withRedirect('/login');
        }

        $_SESSION['user'] = $user->id;
        return $response->withRedirect('/');
    }

    public function logout(Request $request, Response $response, Twig $twig)
    {
        session_destroy();
        return $response->withRedirect('/login');
    }


    private function getUser(string $username, string $password)
    {
        $user = UserModel::where('username', $username)->first();
        if (!isset($user)) {
            return null;
        } else {
            $passwordObj = new Password($password);
            if ($passwordObj->matchPassword($user->password)) {
                return $user;
            } else {
                return null;
            }
        }
    }

    public function getUsersList(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $users = UserModel::all();

        return $twig->render($response, "users.twig", [
            'user' => $user,
            'users' => $users
        ]);
    }

    public function newUserForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();

        return $twig->render($response, "new_user.twig", [
            'user' => $user
        ]);
    }

    public function newUserFormPost(Request $request, Response $response, Twig $twig)
    {
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $verifyPassword = $request->getParam('confirm_password');

        if ($password == $verifyPassword) {
            $passwordObj = new Password($password);
            $user = new UserModel();
            $user->username = $username;
            $user->password = $passwordObj->hash();
            $user->save();
            return $response->withRedirect('/dashboard/users');
        }

        return $response->withRedirect('/dashboard/users/new');
    }

    public function editUserForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $userToEditId = $request->getAttribute('route')->getArgument('id');

        $userToEdit = UserModel::where('id', $userToEditId)->first();

        return $twig->render($response, "edit_user.twig", [
            'user' => $user,
            'user_to_edit' => $userToEdit
        ]);
    }

    public function editUserFormPost(Request $request, Response $response, Twig $twig)
    {
        $userId = $request->getAttribute('route')->getArgument('id');
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $verifyPassword = $request->getParam('confirm_password');

        if ($password == $verifyPassword) {
            $passwordObj = new Password($password);
            $user = UserModel::where('id', $userId)->first();
            $user->username = $username;
            $user->password = $passwordObj->hash();
            $user->save();
            return $response->withRedirect('/dashboard/users');
        }

        return $response->withRedirect('/dashboard/users/edit/' . $userId);
    }

    public function deleteUser(Request $request, Response $response, Twig $twig)
    {
        $userId = $request->getAttribute('route')->getArgument('id');
        $user = UserModel::where('id', $userId)->first();
        $user->delete();
        return $response->withRedirect('/dashboard/users');
    }

    public function editLoggedUserForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();

        return $twig->render($response, "edit_user.twig", [
            'user' => $user,
            'user_to_edit' => $user
        ]);
    }

    public function editLoggedUserFormPost(Request $request, Response $response, Twig $twig)
    {
        $username = $request->getParam('username');
        $password = $request->getParam('password');
        $verifyPassword = $request->getParam('confirm_password');

        if ($password == $verifyPassword) {
            $passwordObj = new Password($password);
            $user = ACL::getLoggedUser();
            $user->username = $username;
            $user->password = $passwordObj->hash();
            $user->save();
            return $response->withRedirect('/dashboard/users');
        }

        return $response->withRedirect('/dashboard/users/edit/');
    }

}
