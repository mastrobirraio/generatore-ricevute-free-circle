# Generatore ricevute Free Circle

Questo applicativo permette di generare le ricevute per i pagamenti fatti all'associazione [Free Circle](https://www.thefreecircle.org/it/home/). 

## Pre requisiti
* MySQL/MariaDB
* PHP >= 7
* Composer

## Installazione

### Database
* Inserisci i valori richiesti dal file `database/.envexample`
* Rinomina `.envexample` in `.env`. RICORDA DI NON COMMITTARLO SUL GIT
* Inizializza il database:
```
sh database/script/initialize-db.sh
```

### Progettto
* Installa le dipendenze del progetto
```
composer install
```

## Avvia applicazione
```
php -S localhost:8080 -t public/
```
NB: Puoi utilizzare sia nginx che Apache apportando le giuste configurazioni

## Reset del database
```
sh database/script/clean-db.sh
```

## Costruito con
* [Composer](https://getcomposer.org/) come dependency manager per PHP
* [Eloquent](https://laravel.com/docs/5.8/eloquent#introduction) com ORM per il database
* [Twig](https://twig.symfony.com/) - come View Engine
* [PHP DI](https://github.com/PHP-DI/PHP-DI) per la dependency injection
* [shSQL](https://github.com/mastrobirraio/shSQL) come ambiente per il database

## Contribuisci
Per favore leggi [CONTRIBUTING.md](CONTRIBUTING.md) per i dettagli sul processo di contrubuzione.

## Autori
* [Pino mastrobirraio Matranga](https://github.com/mastrobirraio) - Lavoro iniziale: matrangagiuseppe99@gmail.com

## Licenza
Questo progetto è sotto licenza MIT - vedi [LICENSE.md](LICENSE.md) per i dettagli

## Riconoscimenti
* [Base skeleton](https://github.com/dantodev/slim-skeleton)
* [Slim project](http://www.slimframework.com/)