<?php
use Illuminate\Database\Capsule\Manager;

/**
 * @var \DI\Container $c
 */

$c->set(\Dtkahl\SimpleConfig\Config::class, DI\factory(new \App\Factory\ConfigFactory));
$c->set(\Monolog\Logger::class, DI\factory(new \App\Factory\MonologFactory));
$c->set(\Slim\Views\Twig::class, DI\factory(new \App\Factory\TwigFactory));
$c->set(\Dtkahl\FileCache\Cache::class, DI\factory(new \App\Factory\CacheFactory));

$capsule = new Manager;
$capsule->addConnection([
    "driver" => getenv("DB_DRIVER"),
    "host" => getenv("DB_HOST"),
    "port" => getenv("DB_PORT"),
    "database" => getenv("DB_NAME"),
    "username" => getenv("DB_USER"),
    "password" => getenv("DB_PASS"),
    'charset' => 'utf8',  
    'collation' => 'utf8_unicode_ci',
]);
$capsule->bootEloquent();
$capsule->setAsGlobal();
