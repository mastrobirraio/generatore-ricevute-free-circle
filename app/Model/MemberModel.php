<?php namespace App\Model;

use Illuminate\Support\Facades\DB;

use App\Model;

class MemberModel extends BaseModel
{
    protected $table = 'members';
}