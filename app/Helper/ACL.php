<?php namespace App\Helper;

use App\Model\UserModel;

class ACL
{

	static function getLoggedUser() {
        $user = UserModel::where('id', $_SESSION['user'])->first();
        return $user;
	}

	static function isLogged() {
        return isset($_SESSION['user']);
	}
}