<?php namespace App\Helper;

class Password
{
    private $password;

    public function __construct(string $password)
    {
        $this->password = $password;
    }

    public function cleanPassword()
    {
        $this->password = strip_tags($this->password);
        $this->cleanPassword();
    }

    public function matchPassword(string $hash)
    {
        return password_verify($this->password, $hash);
    }

    public function hash()
    {
        return password_hash($this->password, PASSWORD_BCRYPT);
    }
}