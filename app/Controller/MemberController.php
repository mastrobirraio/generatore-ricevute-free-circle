<?php namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

use App\Helper\ACL;
use App\Model\MemberModel;

class MemberController
{

    public function getMembersList(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $members = MemberModel::all();

        return $twig->render($response, "members.twig", [
            'user' => $user,
            'members' => $members
        ]);
    }

    public function newMemberForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();

        return $twig->render($response, "new_member.twig", [
            'user' => $user
        ]);
    }

    public function newMemberFormPost(Request $request, Response $response, Twig $twig)
    {
        $name = $request->getParam('name');
        $surname = $request->getParam('surname');
        $CF = $request->getParam('CF');
        $address = $request->getParam('address');

        $member = new MemberModel();
        $member->name = $name;
        $member->surname = $surname;
        $member->CF = strtoupper($CF);
        $member->address = $address;
        $member->save();

        return $response->withRedirect('/dashboard/members');
    }

    public function editMemberForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $memberId = $request->getAttribute('route')->getArgument('id');
        $member = MemberModel::where('id', $memberId)->first();
        print_r($member);
        return $twig->render($response, "edit_member.twig", [
            'user' => $user,
            'member' => $member
        ]);
    }

    public function editMemberFormPost(Request $request, Response $response, Twig $twig)
    {
        $memberId = $request->getAttribute('route')->getArgument('id');

        $name = $request->getParam('name');
        $surname = $request->getParam('surname');
        $CF = $request->getParam('CF');
        $address = $request->getParam('address');

        $member = MemberModel::where('id', $memberId)->first();
        $member->name = $name;
        $member->surname = $surname;
        $member->CF = strtoupper($CF);
        $member->address = $address;
        $member->save();

        return $response->withRedirect('/dashboard/members');
    }

    public function deleteMember(Request $request, Response $response, Twig $twig)
    {
        $memberId = $request->getAttribute('route')->getArgument('id');

        $member = MemberModel::where('id', $memberId)->first();
        $member->delete();

        return $response->withRedirect('/dashboard/members');
    }
}
