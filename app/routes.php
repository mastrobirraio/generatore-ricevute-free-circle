<?php
/** @var \Slim\App $app */

// User Controller Routes
$app->get("/login", ['\App\Controller\UserController', 'getLogin']);
$app->get("/logout", ['\App\Controller\UserController', 'logout']);
$app->get("/dashboard/users", ['\App\Controller\UserController', 'getUsersList']);
$app->get("/dashboard/users/new", ['\App\Controller\UserController', 'newUserForm']);
$app->get("/dashboard/users/edit/[{id}]", ['\App\Controller\UserController', 'editUserForm']);
$app->get("/dashboard/users/delete/[{id}]", ['\App\Controller\UserController', 'deleteUser']);
$app->get("/dashboard/user/edit", ['\App\Controller\UserController', 'editLoggedUserForm']);
$app->post("/login", ['\App\Controller\UserController', 'postLogin']);
$app->post("/dashboard/users/new", ['\App\Controller\UserController', 'newUserFormPost']);
$app->post("/dashboard/users/edit/[{id}]", ['\App\Controller\UserController', 'editUserFormPost']);
$app->post("/dashboard/user/edit", ['\App\Controller\UserController', 'editLoggedUserFormPost']);

// Main Controller Routes
$app->get("/", ['\App\Controller\MainController', 'getHome']);
$app->get("/dashboard/info", ['\App\Controller\MainController', 'getInfo']);
$app->get("/dashboard/invoice/new", ['\App\Controller\MainController', 'getInvoiceForm']);
$app->post("/dashboard/info", ['\App\Controller\MainController', 'infoPost']);
$app->post("/dashboard/invoice/new", ['\App\Controller\MainController', 'getInvoiceFormPost']);

// Member Controller Routes
$app->get("/dashboard/members", ['\App\Controller\MemberController', 'getMembersList']);
$app->get("/dashboard/members/new", ['\App\Controller\MemberController', 'newMemberForm']);
$app->get("/dashboard/members/edit/[{id}]", ['\App\Controller\MemberController', 'editMemberForm']);
$app->get("/dashboard/members/delete/[{id}]", ['\App\Controller\MemberController', 'deleteMember']);
$app->post("/dashboard/members/new", ['\App\Controller\MemberController', 'newMemberFormPost']);
$app->post("/dashboard/members/edit/[{id}]", ['\App\Controller\MemberController', 'editMemberFormPost']);
