--
-- Database: `free-circle`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `users` (
    `id` INT(2) AUTO_INCREMENT NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(100) NOT NULL,
    `created_at` TIMESTAMP NULL DEFAULT current_timestamp(),
    `updated_at` TIMESTAMP NULL DEFAULT current_timestamp(),
    `deleted_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(1) DEFAULT 1,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(150) NOT NULL,
  `zipcode` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `cf` VARCHAR(45) NOT NULL,
  `treasurer` VARCHAR(100) NOT NULL,
  `bank_name` VARCHAR(50) NOT NULL,
  `cc_number` VARCHAR(50) DEFAULT NULL,
  `ABI` VARCHAR(10) DEFAULT NULL,
  `CAB` VARCHAR(10) DEFAULT NULL,
  `CIN` VARCHAR(1) DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `updated_at` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
  `deleted` TIMESTAMP NULL DEFAULT NULL
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(255) AUTO_INCREMENT NOT NULL ,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `CF` varchar(16) NOT NULL,
  `address` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;