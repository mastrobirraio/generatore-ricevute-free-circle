#!/bin/bash

#
# Script to login into database
#

# GETTING CURRENT DIRECTORY
DIR=$(dirname "$0")
. $DIR/../.env

# COLORS
DEFAULT='\e[00m'
YELLOW='\e[33m'
GRAY='\e[90m'

# FUNCTIONS
format_text () {
    COLOR=$1
    TEXT=$2

    echo -e "${COLOR}${TEXT}${DEFAULT}"
}

# SCRIPT STARTS HERE
format_text $YELLOW "Logging"
mysql -u ${ENV_USER} -p${ENV_PASS} ${ENV_NAME}
format_text $GRAY "End process\n\n\n"