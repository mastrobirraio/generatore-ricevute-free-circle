<?php namespace App\Controller;

use App\Model\MemberModel;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

use App\Helper\ACL;
use App\Model\InfoModel;

class MainController
{

    public function getHome(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        return $twig->render($response, 'home.twig', [
            'user' => $user
        ]);
    }

    public function getInfo(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $info = InfoModel::all();

        return $twig->render($response, 'info.twig', [
            'user' => $user,
            'info' => $info[0]
        ]);
    }

    public function infoPost(Request $request, Response $response, Twig $twig)
    {
        $info = InfoModel::where('id', 1)->first();
        $name = $request->getParam('name');
        $address = $request->getParam('address');
        $zipcode = $request->getParam('zipcode');
        $city = $request->getParam('city');
        $cf = $request->getParam('cf');
        $treasurer = $request->getParam('treasurer');
        $bank_name = $request->getParam('bank_name');
        $cc_number = $request->getParam('cc_number');
        $ABI = $request->getParam('ABI');
        $CAB = $request->getParam('CAB');
        $CIN = $request->getParam('CIN');

        $info->name = $name;
        $info->address = $address;
        $info->zipcode = $zipcode;
        $info->city = $city;
        $info->cf = $cf;
        $info->treasurer = $treasurer;
        $info->bank_name = $bank_name;
        $info->cc_number = $cc_number;
        $info->ABI = $ABI;
        $info->CAB = $CAB;
        $info->CIN = $CIN;
        $info->save();
        return $response->withRedirect('/dashboard/info');
    }

    public function getInvoiceForm(Request $request, Response $response, Twig $twig)
    {
        $user = ACL::getLoggedUser();
        $members = MemberModel::all();

        return $twig->render($response, 'invoice_form.twig', [
            'user' => $user,
            'members' => $members
        ]);
    }

    public function getInvoiceFormPost(Request $request, Response $response, Twig $twig)
    {
        $invoice_title = strtoupper($request->getParam('invoice_title'));
        $invoice_number = $request->getParam('invoice_number');
        $invoice_year = $request->getParam('invoice_year');
        $invoice_amount = $request->getParam('invoice_amount');
        $invoice_amount_in_words = $request->getParam('invoice_amount_in_words');
        if ($request->getParam('receiving_type') == 'no_member') {
            $name = $request->getParam('receiving_name');
            $surname = $request->getParam('receiving_surname');
            $address = $request->getParam('receiving_address');
            $CF = strtoupper($request->getParam('receiving_CF'));
        } else if ($request->getParam('receiving_type') == 'member') {
            $member = MemberModel::where('id', $request->getParam('member_id'))->first();
            $name = $member->name;
            $surname = $member->surname;
            $address = $member->address;
            $CF = $member->CF;
        }
        $payment_type = $request->getParam('payment_type');
        $bank_check_number = $request->getParam('bank_check_number');
        $bank_name = $request->getParam('bank_name');
        $invoice_date = $request->getParam('invoice_date');

        $info = InfoModel::where('id', 1)->first();
        $user = ACL::getLoggedUser();

        return $twig->render($response, 'invoice.twig', [
            'user' => $user,
            'info' => $info,
            'invoice_title' => $invoice_title,
            'invoice_number' => $invoice_number,
            'invoice_year' => $invoice_year,
            'invoice_amount' => $invoice_amount,
            'invoice_amount_in_words' => $invoice_amount_in_words,
            'name' => $name,
            'surname' => $surname,
            'address' => $address,
            'CF' => $CF,
            'payment_type' => $payment_type,
            'banck_check_number' => $bank_check_number,
            'bank_name' => $bank_name,
            'invoice_date' => $invoice_date
        ]);
    }
}
