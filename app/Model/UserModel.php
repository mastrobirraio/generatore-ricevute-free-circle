<?php namespace App\Model;

use Illuminate\Support\Facades\DB;

use App\Model;

class UserModel extends BaseModel 
{
    protected $table = 'users';
}