<?php namespace App\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

use App\Helper\ACL;

class UserLoggedMiddleware
{
    public function __invoke(Request $request, Response $response, callable $next)
    {
        switch ($request->getUri()->getPath()) {
            case '/login':
                break;
            default:
                if(!ACL::isLogged()) {
                    return $response->withRedirect('/login');
                }
                break;
        }
        $response = $next($request, $response);
        return $response;
    }
}