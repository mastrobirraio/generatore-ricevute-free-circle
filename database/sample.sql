--
-- Database: `free-circle`
--

-- --------------------------------------------------------

--
-- Data for table `user`
--
INSERT INTO `users` (`username`, `password`) VALUES ('superuser', '$2y$12$EX1x2gBycXIDni.TTW3hA.huKVqV4mgKTFUB12RMazKeJf9OEqPuS');

--
-- Data for table `info`
--
INSERT INTO `info` (`name`, `address`, `zipcode`, `city`, `cf`, `treasurer`, `bank_name`, `cc_number`, `ABI`, `CAB`, `CIN`, `created_at`, `updated_at`, `deleted`) VALUES
('Free Circle A.P.S.', 'Viale Regione Siciliana, 2396', '90135', 'Palermo', '97330160827', 'Antonino Ventimiglia', 'Credito Siciliano', '00000000B258', '030119', '04603', '0', '2018-07-07 08:26:20', '2018-07-07 08:26:20', NULL);